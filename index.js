const ToCsv = require("./src/sqlite-to-csv");
const csvToJson = require("csv-file-to-json");
require('dotenv').config()

let filePath = process.env.PATH_DB_SQLITE_FILE;
let outputPath = process.env.PATH_SAVE_CSV;
let tableName = process.env.TABLE_NAME;
let logPath = process.env.ROOT_PATH;
let sqliteToCsv = new ToCsv()
    .setFilePath(filePath)
    .setOutputPath(outputPath)
    .setLogPath(logPath);

sqliteToCsv.convert().then((result) => {
    console.log("Convert to CSV successfully")

    const dataInJSON = csvToJson({ filePath: `${outputPath}${tableName}.csv`});

    var sqlite3 = require('@journeyapps/sqlcipher').verbose();
    var db = new sqlite3.Database(`${process.env.PATH_SAVE_SQLCIPHER}sqlcipher.db`);
    db.serialize(function () {
        db.run("PRAGMA cipher_compatibility = 4");
        db.run(`PRAGMA key = ${process.env.PASSWORD_SQLCIPHER}`);
        db.run(`DROP TABLE IF EXISTS ${tableName}`)
        db.run(`CREATE TABLE ${tableName} (
            'ogc_fid' INTEGER, 
            'GEOMETRY' BLOB, 
            'name' VARCHAR(254),
            'latitude' FLOAT,
            'longitude' FLOAT,
            'speed_max1' INTEGER,
            'bearing' FLOAT,
            'note' INTEGER,
            'st_name_sy' VARCHAR(254),
            'id_sy'	BIGINT,
            'type'	VARCHAR(10),
            PRIMARY KEY(ogc_fid AUTOINCREMENT)
            )`);
        var stmt = db.prepare(`INSERT INTO ${tableName} VALUES (?, ?, ?, ?, ?, ?, ?, ?, ? ,?, ?)`);
        for (var i = 0; i < dataInJSON.length; i++) {
            stmt.run(dataInJSON[i].ogc_fid, dataInJSON[i].GEOMETRY, dataInJSON[i].name,
                dataInJSON[i].latitude, dataInJSON[i].longitude, dataInJSON[i].speed_max1,
                dataInJSON[i].bearing, dataInJSON[i].note, dataInJSON[i].st_name_sy, dataInJSON[i].id_sy, dataInJSON[i].type);
        }
        stmt.finalize();

        db.each(`SELECT rowid AS ogc_fid, name FROM ${tableName}`, function (err, row) {
            console.log("- ogc_fid: " + row.ogc_fid + ", " +" name: " + row.name);
        });
    });
    db.close();
}).catch((err) => {
    console.log("Converted false", err.toString())
});


// const express = require('express');
// const app = express();
// const path = require('path');


// app.get('/', (req,res) => {
//   res.sendFile(path.join(__dirname+'/src/views/index.html'));
  
// });

// app.listen(8089, () => {
//     console.log('Listening on port 8089');
// });